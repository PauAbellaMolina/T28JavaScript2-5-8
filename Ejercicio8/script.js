var num = 4;

window.onload = printearResultado;

function printearResultado() {
    document.getElementById('numero').innerHTML = num;
    document.getElementById('resultado').innerHTML = determinarNumero(num);
}

function determinarNumero(numero) {
    if(numero % 2 == 0) {
        return "par";
    } else {
        return "impar";
    }
}